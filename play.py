import numpy as np
import pandas as pd
import time
from pprint import pprint




class Game_Sudoku:
    def __init__(self, input_schema):
        self.lattices = np.genfromtxt(input_schema, delimiter=',')
        print(self.lattices)
        #self.lattices = pd.read_csv('schema-1.csv', sep=",", header=None).values
        self.all_possible_set = {1, 2, 3, 4, 5, 6, 7, 8, 9}
        self.lattices_need = dict()
        self.row_need = dict()
        self.col_need = dict()
        self.nine_squares = self.get_nine_squares()
        
        self.board_shape = list(np.shape(self.lattices))
        for i in range(self.board_shape[0]):
            self.lattices_need[str(i)] = dict()
            for j in range(self.board_shape[1]):
                self.lattices_need[str(i)][str(j)] = set()
        
        print(self.lattices_need['1']['2'])
        
        for i in range(self.board_shape[0]):
            self.row_need[str(i)] = set()
        for j in range(self.board_shape[1]):
            self.col_need[str(j)] = set()
    
    
    def get_nine_squares(self):
        nine_squares = list()
        for i in range(3):
            for j in range(3):
                print(self.lattices[ i*3 : (i+1)*3,  j*3 : (j+1)*3 ])
                square_rows = self.lattices[ i*3 : (i+1)*3,  j*3 : (j+1)*3 ]
                set_of_square = set()
                for row in square_rows:
                    set_of_square.update(row)
                nine_squares.append(set_of_square)
        return nine_squares
        
        
    def mapping_lattice_on_square(self, row, col):
        row = int(row) 
        col = int(col)
        if row < 3 and col < 3:
            return 0
        elif row < 3 and 3 <= col < 6:
            return 1
        elif row < 3 and 6 <= col < 9:
            return 2
        
        elif 3 <= row < 6 and col < 3:
            return 3
        elif 3 <= row < 6 and 3 <= col < 6:
            return 4
        elif 3 <= row < 6 and 6 <= col < 9:
            return 5
        
        elif 6 <= row < 9 and col < 3:
            return 6
        elif 6 <= row < 9 and 3 <= col < 6:
            return 7
        elif 6 <= row < 9 and 6 <= col < 9:
            return 8
        
        else:
            raise False
    
    
    def remaining_in_nine_square(self, row, col):
        n = self.mapping_lattice_on_square(row, col)
        #print(f"n: {n}")
        return self.all_possible_set - set(self.nine_squares[n])
        
    
    def remaining_at_a_line(self, direct, line):
        if direct == 'row':
            return self.all_possible_set - set(line)
        elif direct == 'col':
            return self.all_possible_set - set(line)
        else:
            assert False
            
    
    def guess_the_lattices(self):
        for row in range(self.board_shape[0]):
            for col in range(self.board_shape[1]):
                if self.lattices[row, col] == 0:
                    need_in_row = self.remaining_at_a_line(
                        direct='row', 
                        line=self.lattices[row, :], 
                    )
                    need_in_col = self.remaining_at_a_line(
                        direct='col', 
                        line=self.lattices[:, col], 
                    )
                    #print(row, col)
                    need_in_square = self.remaining_in_nine_square(row, col)
                    
                    self.lattices_need[str(row)][str(col)] = need_in_row & need_in_col & need_in_square
                    
                    #print()
                    if len(self.lattices_need[str(row)][str(col)]) == 1:
                        the_one = self.lattices_need[str(row)][str(col)].pop()
                        self.lattices[row, col] = the_one
                        n = self.mapping_lattice_on_square(row, col)
                        self.nine_squares[n].add(the_one)
                        
                        
                
                        
        return self.lattices
        
    
    
if __name__=="__main__":
    game = Game_Sudoku(input_schema='schema-1.csv')
    count = 0
    while True:
        print(f"\n=== Round {count+1} ===")
        lattices = game.guess_the_lattices()
        print(np.array(lattices))
        #pprint(game.lattices_need)
        #time.sleep(0.5)
        count += 1
        
        if count == 5:
            break